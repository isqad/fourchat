-- Deploy ladung:add_uniq_constraint_to_user_photos to pg

BEGIN;
  ALTER TABLE user_photos ADD CONSTRAINT uniq_user_photos_user_id UNIQUE USING INDEX index_user_photos;
COMMIT;
