package domain

import (
	"github.com/jmoiron/sqlx"
	"time"
)

type DialogRequest struct {
	Id              int64     `json:"id"`
	UserId          int64     `json:"user_id" db:"user_id"`
	RelatedUserName string    `json:"related_user_name,omitempty" db:"-"`
	RelatedUserId   NullInt64 `json:"related_user_id,omitempty" db:"related_user_id"`
	Gender          string    `json:"gender"`
	AgeFrom         int       `json:"age_from" db:"age_from"`
	AgeTo           int       `json:"age_to" db:"age_to"`
	CreatedAt       time.Time `json:"-" db:"created_at"`

	db *sqlx.DB
}

func NewDialogRequest(db *sqlx.DB) *DialogRequest {
	return &DialogRequest{db: db}
}

func (r *DialogRequest) Save() error {
	var lastInsertId int64

	insertSql := `INSERT INTO requests (user_id, age_from, age_to, gender, created_at)
				  VALUES ($1, $2, $3, $4, NOW()) RETURNING id`

	err := r.db.Get(&lastInsertId, insertSql, r.UserId, r.AgeFrom, r.AgeTo, r.Gender)
	if err != nil {
		return err
	}

	r.Id = lastInsertId

	return nil
}

func (r *DialogRequest) Del() error {
	deleteSql := `
	  WITH reqs AS (
		SELECT ARRAY[req1.id, COALESCE(req2.id, 0)] AS ids FROM requests req1
		LEFT JOIN requests req2 ON req2.related_user_id = req1.user_id AND req2.user_id = req1.related_user_id
		  WHERE req1.id = $1
		) DELETE FROM requests WHERE id = ANY((TABLE reqs)::bigint[])`

	if _, err := r.db.Exec(deleteSql, r.Id); err != nil {
		return err
	}

	/**
	eventBus := r.AppContext.EventBus

	eventForUser := EventReq{Type: EVENT_DELETE, UserId: r.UserId, Payload: DialogRequest{Id: r.Id}}
	eventMessageForUser, err := json.Marshal(eventForUser)
	if err != nil {
		return err
	}
	eventBus.Pub <- eventMessageForUser

	if relatedDialogId != 0 {
		eventForRelatedUser := EventReq{Type: EVENT_DELETE, UserId: r.RelatedUserId.Int64, Payload: DialogRequest{Id: relatedDialogId}}
		eventMessageForRelatedUser, err := json.Marshal(eventForRelatedUser)
		if err != nil {
			return err
		}
		eventBus.Pub <- eventMessageForRelatedUser
	}
	**/

	return nil
}
