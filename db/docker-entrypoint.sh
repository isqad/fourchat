#!/bin/sh

cd /src
export PGPASSWORD="$PGDATABASE_PASSWORD"

# we will generate sqitch.conf here we have only production section
cat > sqitch.conf << EOL
[core]
    engine = pg
[target "default"]
    uri = db:pg://$PGDATABASE_USER:$PGPASSWORD@$PGDATABASE_HOST/$PGDATABASE_NAME
EOL
# waiting for postgres up
while ! psql -h$PGDATABASE_HOST -U$PGDATABASE_USER -d$PGDATABASE_NAME -c "select 1" > /dev/null
do
    echo "$(date) - still trying to connect to PostgreSQL host: $PGDATABASE_HOST, user: $PGDATABASE_USER, password: $PGDATABASE_PASSWORD, database: $PGDATABASE_NAME"
    sleep 5
done
echo "$(date) - connected successfully"

$@
