-- Deploy ladung:add_fk_requests_related_user_id to pg

BEGIN;

  ALTER TABLE requests ADD CONSTRAINT fk_requests_related_user_id_users FOREIGN KEY(related_user_id) REFERENCES users(id)
    ON DELETE SET NULL;

  CREATE UNIQUE INDEX index_requests_user_id_related_user_id ON requests (user_id, related_user_id);
  CREATE INDEX index_requests_related_user_id ON requests (related_user_id);

COMMIT;
