package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
	"os"
	"testing"
)

func TestMain(t *testing.T) {
	dbSpec := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		os.Getenv("FOURCHAT_APP_POSTGRES_USER"),
		os.Getenv("FOURCHAT_APP_POSTGRES_PASSWORD"),
		os.Getenv("FOURCHAT_APP_POSTGRES_HOST"),
		os.Getenv("FOURCHAT_APP_POSTGRES_PORT"),
		os.Getenv("FOURCHAT_APP_POSTGRES_DB"),
	)

	db, err := sqlx.Connect("postgres", dbSpec)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
}
