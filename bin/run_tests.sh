#!/bin/bash

set -e

project_name=test
compose_file=docker-compose.test.yml
compose_cmd="docker-compose -f ${compose_file} -p ${project_name}"

# Гасим все окружение
${compose_cmd} down -v
# Миграции
${compose_cmd} run --rm fourchat-sqitch sqitch deploy default
# Тесты
${compose_cmd} run --rm fourchat-app go test -v -cover ./...
