-- Deploy ladung:add_uniq_users_constaint to pg

BEGIN;

  ALTER TABLE users ADD CONSTRAINT uniq_users_uuid UNIQUE USING INDEX index_users_on_uuid;

COMMIT;
