-- Revert ladung:add_fk_requests_related_user_id from pg

BEGIN;

  DROP INDEX index_requests_user_id_related_user_id;
  DROP INDEX index_requests_related_user_id;
  ALTER TABLE requests DROP CONSTRAINT fk_requests_related_user_id_users;

COMMIT;
