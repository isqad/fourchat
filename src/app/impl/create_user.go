package impl

import (
	"encoding/json"
	"gitlab.com/isqad/fourchat/domain"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"
	"go.uber.org/zap"
	"net/http"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	db, _ := appMiddleware.GetDb(r.Context())
	logger, _ := appMiddleware.GetLog(r.Context())

	user := domain.NewUser(db)
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		logger.Error("decoding request body failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user.UserAgent = r.UserAgent()
	user.Ip = r.RemoteAddr

	if err := user.Save(); err != nil {
		logger.Error("saving user failed", zap.Error(err))
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	resp, err := json.Marshal(user)
	if err != nil {
		logger.Error("marshaling user failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
