package impl

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/isqad/fourchat/domain"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"
	"go.uber.org/zap"
	"net/http"
)

func ShowUser(w http.ResponseWriter, r *http.Request) {
	db, _ := appMiddleware.GetDb(r.Context())
	logger, _ := appMiddleware.GetLog(r.Context())

	uuid := chi.URLParam(r, "uuid")
	user, err := domain.NewUser(db).Find(uuid)
	if err != nil {
		logger.Error("Error during query", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if user == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	resp, err := json.Marshal(user)
	if err != nil {
		logger.Error("marshaling user failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
