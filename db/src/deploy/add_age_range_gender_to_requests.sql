-- Deploy ladung:add_age_range_gender_to_requests to pg

BEGIN;

  ALTER TABLE requests ADD COLUMN age_from integer NOT NULL DEFAULT 18,
                       ADD COLUMN age_to integer NOT NULL DEFAULT 40,
                       ADD COLUMN gender gender_type NOT NULL DEFAULT 'female';

COMMIT;
