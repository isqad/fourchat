-- Deploy ladung:add_photo_to_users to pg

BEGIN;

  CREATE TABLE user_photos (
    id bigserial PRIMARY KEY NOT NULL,
    file_name varchar(1024) NOT NULL,
    user_id bigint
  );

  ALTER TABLE user_photos ADD CONSTRAINT fk_user_photos_user_id_users FOREIGN KEY(user_id) REFERENCES users(id);

  CREATE UNIQUE INDEX index_user_photos ON user_photos (user_id);

COMMIT;
