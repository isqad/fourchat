package domain

import (
	"github.com/google/uuid"
	"testing"
)

func TestFind(t *testing.T) {
	d := db()
	// Load fixtures
	user := NewUser(d)
	uuid := uuid.New().String()

	d.MustExec(`INSERT INTO users
	  (uuid, name, gender, age, jwt, user_agent, ip, created_at, updated_at) VALUES
	  ($1,   $2,   $3,     $4,  $5,  $6,         $7, NOW(), NOW())`,
		uuid, "Alex", "female", 25, "foo-bar", "Chromium", "127.0.0.2",
	)

	u, err := user.Find(uuid)
	if err != nil {
		t.Errorf("Expected no errors, but got: %v", err)
	}

	if u.Uuid != uuid {
		t.Errorf("Expected user has uuid, but not: %v", u)
	}
	if u.Name != "Alex" {
		t.Errorf("Expected user has name, but not: %v", u)
	}
	if u.Gender != "female" {
		t.Errorf("Expected user has gender, but not: %v", u)
	}
	if u.Age != 25 {
		t.Errorf("Expected user has age, but not: %v", u)
	}
	if u.Jwt != "foo-bar" {
		t.Errorf("Expected user has jwt, but not: %v", u)
	}

	u, err = user.Find("xxxx")
	if err != nil {
		t.Errorf("Expected no errors, but got: %v", err)
	}
	if u != nil {
		t.Errorf("Expected result nil, but got: %v", u)
	}
}

func TestUserSave(t *testing.T) {
	d := db()
	user := NewUser(d)
	user.Name = "Alex"
	user.Age = 56
	user.Gender = "male"
	user.Ip = "192.168.1.1"
	user.UserAgent = "Chromium"

	err := user.Save()
	if err != nil {
		t.Errorf("Expected no errors, but got: %v", err)
	}

	if user.Id == 0 {
		t.Errorf("Expected user has id, but no: %v", user)
	}

	if user.Jwt == "" {
		t.Errorf("Expected user has jwt, but no: %v", user)
	}

	if user.Uuid == "" {
		t.Errorf("Expected user has uuid, but no: %v", user)
	}

	user2 := NewUser(d)
	user2.Name = "Alex2"
	user2.Age = 25
	user2.Gender = "male"
	user2.Ip = "192.168.1.1"
	user2.UserAgent = "Chromium"
	user2.Uuid = user.Uuid

	err = user2.Save()
	if err != nil {
		t.Errorf("Expected no errors, but got: %v", err)
	}
}
