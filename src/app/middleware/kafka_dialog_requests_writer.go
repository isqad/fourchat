package middleware

import (
	"context"
	"github.com/go-chi/chi/middleware"
	"log"
	"net/http"
	"os"

	kafka "github.com/segmentio/kafka-go"
)

const (
	DialogRequestsTopic = "KafkaDialogRequests"
)

var (
	ctxKeyKafkaDialogRequestWriter = contextKey(DialogRequestsTopic)
)

func NewKafkaDialogRequestsWriter(errorLogger *log.Logger) *kafka.Writer {
	return kafka.NewWriter(kafka.WriterConfig{
		Brokers:     []string{os.Getenv("FOURCHAT_APP_KAFKA_URL")},
		Topic:       DialogRequestsTopic,
		Balancer:    &kafka.LeastBytes{},
		Async:       true,
		ErrorLogger: errorLogger,
	})
}

func GetKafkaWriter(ctx context.Context, topic string) (*kafka.Writer, bool) {
	w, ok := ctx.Value(contextKey(topic)).(*kafka.Writer)
	return w, ok
}

func KafkaDialogRequestsWriter(k *kafka.Writer) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		h := func(w http.ResponseWriter, r *http.Request) {
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			r = r.WithContext(context.WithValue(r.Context(), ctxKeyKafkaDialogRequestWriter, k))
			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(h)
	}
}
