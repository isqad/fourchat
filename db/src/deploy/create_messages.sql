-- Deploy ladung:create_messages to pg

BEGIN;

  CREATE TABLE messages (
    id bigserial NOT NULL PRIMARY KEY,
    user_id bigint NOT NULL,
    request_id bigint,
    body text,
    created_at timestamp with time zone NOT NULL
  );

  CREATE INDEX index_messages_user_id ON messages (user_id);
  CREATE INDEX index_messages_request_id ON messages (request_id);

  ALTER TABLE messages ADD CONSTRAINT fk_messages_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;
  ALTER TABLE messages ADD CONSTRAINT fk_messages_request_id FOREIGN KEY (request_id) REFERENCES requests(id) ON DELETE SET NULL;

COMMIT;
