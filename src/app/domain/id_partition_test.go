package domain

import (
	"strings"
	"testing"
)

func TestIdPartition(t *testing.T) {
	got, err := IdPartition(1)

	if strings.Join(got, `/`) != "000/000/001" {
		t.Errorf("IdPartition(1) = %s; want 000/000/001", got)
	}

	got, err = IdPartition(-1)
	if err == nil {
		t.Errorf("IdPartition(-1) should return error")
	}

	got, err = IdPartition(int64(1234567891234568))
	if strings.Join(got, `/`) != "123/456/789/123/456/8" {
		t.Errorf("IdPartition(1234567891234568) = %s; want 123/456/789/123/456/8", got)
	}

	got, err = IdPartition(int64(12345678912))
	if strings.Join(got, `/`) != "123/456/789/12" {
		t.Errorf("IdPartition(12345678912) = %s; want 123/456/789/12", got)
	}
}
