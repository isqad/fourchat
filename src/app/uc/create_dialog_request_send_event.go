package uc

import (
	"context"
	"encoding/json"
	"gitlab.com/isqad/fourchat/domain"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"

	kafka "github.com/segmentio/kafka-go"
)

func CreateDialogRequestSendEvent(ctx context.Context, dialogReq *domain.DialogRequest) error {
	kafkaWriter, _ := appMiddleware.GetKafkaWriter(ctx, appMiddleware.DialogRequestsTopic)
	event := domain.NewDialogRequestCreateType(dialogReq)

	eventJson, err := json.Marshal(event)
	if err != nil {
		return err
	}

	msg := kafka.Message{
		Key:   []byte(event.Key()),
		Value: eventJson,
	}
	err = kafkaWriter.WriteMessages(ctx, msg)
	if err != nil {
		return err
	}

	return nil
}
