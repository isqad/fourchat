package domain

import (
	"errors"
	"fmt"
)

const PART_LEN = 3

func IdPartition(id int64) ([]string, error) {
	if id <= 0 {
		return nil, errors.New("Id must be great or eq zero")
	}

	var parts []string

	idStr := fmt.Sprintf("%09d", id)

	acc := ""
	idStrLen := len(idStr)
	tailLen := idStrLen
	nextPos := 0

	for pos, c := range idStr {
		acc += string(c)
		nextPos = pos + 1

		if nextPos%PART_LEN == 0 {
			parts = append(parts, acc)
			acc = ""
			tailLen -= PART_LEN
		} else if tailLen > 0 && tailLen < PART_LEN {
			parts = append(parts, idStr[pos:idStrLen])
			break
		}
	}

	return parts, nil
}
