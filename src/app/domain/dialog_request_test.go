package domain

import (
	"database/sql"
	"testing"
)

func TestDialogRequestSave(t *testing.T) {
	d := db()

	// Load fixtures
	u, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	r := NewDialogRequest(d)
	r.UserId = u.Id
	r.AgeFrom = 20
	r.AgeTo = 25
	r.Gender = "any"

	err = r.Save()
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if r.Id == 0 {
		t.Errorf("Expected request has id, but no: %v", r)
	}
}

func TestDialogRequestDel(t *testing.T) {
	d := db()

	// Load fixtures
	u1, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	u2, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	r, err := DialogRequestFixture(d, u1, u2)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}
	_, err = DialogRequestFixture(d, u2, u1)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	err = r.Del()
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	req := NewDialogRequest(d)
	err = d.Get(req, `SELECT * FROM requests WHERE user_id = $1 OR related_user_id = $1`, u1.Id)
	if err != sql.ErrNoRows {
		t.Errorf("Expected no rows, got: %v, err: %v", req, err)
	}
}
