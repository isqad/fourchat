package domain

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
	"os"
)

func db() *sqlx.DB {
	dbSpec := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		os.Getenv("FOURCHAT_APP_POSTGRES_USER"),
		os.Getenv("FOURCHAT_APP_POSTGRES_PASSWORD"),
		os.Getenv("FOURCHAT_APP_POSTGRES_HOST"),
		os.Getenv("FOURCHAT_APP_POSTGRES_PORT"),
		os.Getenv("FOURCHAT_APP_POSTGRES_DB"),
	)

	db, err := sqlx.Connect("postgres", dbSpec)
	if err != nil {
		log.Fatalf("Opening db connectoin failed: %v", err)
	}

	return db
}

func UserFixture(d *sqlx.DB) (*User, error) {
	u := NewUser(d)
	u.Name = "Bob"
	u.Age = 25
	u.Gender = "male"
	u.Ip = "192.168.1.1"
	u.UserAgent = "curl"

	err := u.Save()
	if err != nil {
		return nil, err
	}

	return u, nil
}

func UserPhotoFixture(d *sqlx.DB, user *User) (*UserPhoto, error) {
	p := NewUserPhoto(d)

	p.FileName = "test.png"
	p.UserId = user.Id

	err := p.Save()
	if err != nil {
		return nil, err
	}

	return p, nil
}

func DialogRequestFixture(d *sqlx.DB, user1 *User, user2 *User) (*DialogRequest, error) {
	r := NewDialogRequest(d)
	r.UserId = user1.Id
	r.RelatedUserId = NullInt64{user2.Id, true}
	r.AgeFrom = 20
	r.AgeTo = 25
	r.Gender = "any"

	err := r.Save()
	if err != nil {
		return nil, err
	}

	return r, nil
}
