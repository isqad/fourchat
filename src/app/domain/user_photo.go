package domain

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const (
	UserPhotoPrefixPath = "/user_photos"
	UserPhotoDir        = "./static/user_photos"
	imgPrefixPath       = "http://img.fourchat.%s/rs/fit/512/512/sm/0/plain/local://%s?%d"
)

type UserPhoto struct {
	Id        int64     `json:"id"`
	UserId    int64     `json:"user_id" db:"user_id"`
	FileName  string    `json:"file_name" db:"file_name"`
	UpdatedAt time.Time `json:"-" db:"updated_at"`

	idPartitions []string
	db           *sqlx.DB
}

func NewUserPhoto(db *sqlx.DB) *UserPhoto {
	return &UserPhoto{db: db}
}

func (p *UserPhoto) Save() error {
	var lastInsertId int64

	insertSql := `INSERT INTO user_photos (user_id, file_name, updated_at)
				  VALUES ($1, $2, NOW()) ON CONFLICT ON CONSTRAINT uniq_user_photos_user_id
				  DO UPDATE SET
				    file_name = EXCLUDED.file_name,
					updated_at = NOW()
				  RETURNING id`

	err := p.db.Get(&lastInsertId, insertSql, p.UserId, p.FileName)
	if err != nil {
		return err
	}

	p.Id = lastInsertId

	return nil
}

func (p *UserPhoto) FindByUserId(id int64) (*UserPhoto, error) {
	findStatement := `SELECT
		id,
		user_id,
		file_name,
		updated_at
	  FROM user_photos WHERE user_id = $1`

	err := p.db.Get(p, findStatement, id)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return p, nil
}

func (p *UserPhoto) PhotoUrl() (string, error) {
	imgPath, err := p.PhotoPath(UserPhotoPrefixPath)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf(imgPrefixPath, os.Getenv("DOCKER_TLD"), imgPath, p.UpdatedAt.Unix()), nil
}

func (p *UserPhoto) PhotoIdPartitions() ([]string, error) {
	if len(p.idPartitions) > 0 {
		return p.idPartitions, nil
	}

	return IdPartition(p.Id)
}

func (p *UserPhoto) PhotoDir(prefixDir string) (string, error) {
	dirs, err := p.PhotoIdPartitions()
	if err != nil {
		return "", err
	}
	return prefixDir + "/" + strings.Join(dirs[:(len(dirs)-1)], `/`), nil
}

func (p *UserPhoto) PhotoPath(prefixDir string) (string, error) {
	photoDir, err := p.PhotoDir(prefixDir)
	if err != nil {
		return "", err
	}
	dirs, err := p.PhotoIdPartitions()
	if err != nil {
		return "", err
	}
	fileName := dirs[len(dirs)-1] + "_original" + filepath.Ext(p.FileName)

	return photoDir + "/" + fileName, nil
}
