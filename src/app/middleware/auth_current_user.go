package middleware

import (
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/isqad/fourchat/domain"
	"go.uber.org/zap"
	"net/http"
	"os"
)

var (
	ctxKeyCurrentUser = contextKey("CurrentUser")
)

func GetCurrentUser(ctx context.Context) (*domain.User, bool) {
	l, ok := ctx.Value(ctxKeyCurrentUser).(*domain.User)
	return l, ok
}

func AuthCurrentUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		signKey := os.Getenv("FOURCHAT_SECRET_KEY")
		authToken := r.Header.Get("X-Authorization")
		logger, _ := GetLog(r.Context())

		if authToken == "" {
			http.Error(w, http.StatusText(401), 401)
			logger.Error("Empty jwt token")
			return
		}

		token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("Error during parse jwt")
			}
			return []byte(signKey), nil
		})
		if err != nil {
			http.Error(w, http.StatusText(401), 401)
			logger.Error("Error during parse jwt", zap.Error(err))
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			if userUuid, ok := claims["sub"].(string); ok {
				db, _ := GetDb(r.Context())
				usr := domain.NewUser(db)
				currentUser, err := usr.Find(userUuid)
				if currentUser == nil {
					logger.Error("Error: can not find user", zap.String("uuid", userUuid))
					http.Error(w, http.StatusText(404), 404)
					return
				}
				if err != nil {
					logger.Error("Error: can not find user", zap.Error(err))
					http.Error(w, http.StatusText(401), 401)
					return
				}

				r = r.WithContext(context.WithValue(r.Context(), ctxKeyCurrentUser, currentUser))
				next.ServeHTTP(w, r)
			} else {
				logger.Error("Error: invalid parse user uuid", zap.Any("claims", claims))
				http.Error(w, http.StatusText(401), 401)
				return
			}
		} else {
			logger.Error("Error: invalid jwt")
			http.Error(w, http.StatusText(401), 401)
			return
		}
	})
}
