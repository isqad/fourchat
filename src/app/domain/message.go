package domain

import (
	"gitlab.com/fourchat/app"
)

type Message struct {
	Id        int64  `json:"id"`
	UserId    int64  `json:"user_id"`
	RequestId int64  `json:"request_id"`
	Body      string `json:"body"`
	CreatedAt int    `json:"created_at"`

	AppContext *app.AppContext
}

func (m *Message) Save() error {
	var lastInsertId int64

	insertSql := `INSERT INTO messages (user_id, request_id, body, created_at)
				  VALUES ($1, $2, $3, NOW()) RETURNING id`

	err := m.AppContext.Db.QueryRow(insertSql, m.UserId, m.RequestId, m.Body).Scan(&lastInsertId)
	if err != nil {
		return err
	}

	m.Id = lastInsertId

	return nil
}
