-- Revert ladung:add_age_range_gender_to_requests from pg

BEGIN;

  ALTER TABLE requests DROP COLUMN age_from, DROP COLUMN age_to, DROP COLUMN gender;

COMMIT;
