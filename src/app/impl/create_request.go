package impl

import (
	"encoding/json"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"
	"gitlab.com/isqad/fourchat/uc"
	"go.uber.org/zap"
	"net/http"
)

func CreateRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	logger, _ := appMiddleware.GetLog(ctx)
	dialogReq, err := uc.CreateDialogRequest(ctx, r.Body)
	if err != nil {
		logger.Error("saving dialog request failed", zap.Error(err))
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	resp, err := json.Marshal(dialogReq)
	if err != nil {
		logger.Error("marshaling dialog request failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = uc.CreateDialogRequestSendEvent(ctx, dialogReq)
	if err != nil {
		logger.Error("failed send message", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}
