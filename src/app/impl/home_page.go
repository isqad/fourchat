package impl

import (
	"html/template"
	"net/http"
)

func HomePage(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("views/homepage.html"))

	tmpl.Execute(w, nil)
}
