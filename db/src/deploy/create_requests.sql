-- Deploy ladung:create_requests to pg

BEGIN;

  CREATE TABLE requests (
    id bigserial NOT NULL PRIMARY KEY,
    user_id bigint NOT NULL,
    related_user_id bigint,
    created_at timestamp with time zone NOT NULL,
    CHECK (related_user_id != user_id)
  );

  ALTER TABLE requests ADD CONSTRAINT fk_users_requests_user_id FOREIGN KEY(user_id) REFERENCES users(id)
    ON DELETE CASCADE;

COMMIT;
