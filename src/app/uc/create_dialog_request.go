package uc

import (
	"context"
	"encoding/json"
	"gitlab.com/isqad/fourchat/domain"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"
	"io"
)

// Создает DialogRequest из httpBody
func CreateDialogRequest(ctx context.Context, httpBody io.ReadCloser) (*domain.DialogRequest, error) {
	user, _ := appMiddleware.GetCurrentUser(ctx)
	db, _ := appMiddleware.GetDb(ctx)

	dialogReq := domain.NewDialogRequest(db)
	decoder := json.NewDecoder(httpBody)

	if err := decoder.Decode(&dialogReq); err != nil {
		return nil, err
	}
	dialogReq.UserId = user.Id

	if err := dialogReq.Save(); err != nil {
		return nil, err
	}

	return dialogReq, nil
}
