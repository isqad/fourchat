package domain

import (
	"database/sql"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"os"
	"time"
)

const (
	JWT_EXPIRES_DAYS = 365
)

type User struct {
	Id        int64     `json:"id"`
	Uuid      string    `json:"uuid"`
	UserAgent string    `json:"user_agent" db:"user_agent"`
	Ip        string    `json:"-"`
	Jwt       string    `json:"jwt"`
	Name      string    `json:"name"`
	Gender    string    `json:"gender"`
	Age       int       `json:"age"`
	PhotoUrl  string    `json:"photo_url" db:"-"`
	CreatedAt time.Time `json:"-" db:"created_at"`
	UpdatedAt time.Time `json:"-" db:"updated_at"`

	db *sqlx.DB
}

func NewUser(db *sqlx.DB) *User {
	return &User{db: db}
}

func (u *User) Save() error {
	if err := u.validate(); err != nil {
		return err
	}

	var lastInsertId int64

	if u.Uuid == "" {
		u.Uuid = uuid.New().String()
	}

	if err := u.RefreshToken(); err != nil {
		return err
	}

	insertSql := `INSERT INTO users (
	  uuid,
	  user_agent,
	  ip,
	  name,
	  gender,
	  created_at,
	  updated_at,
	  age,
	  jwt
	) VALUES ($1, $2, $3, $4, $5, NOW(), NOW(), $6, $7) ON CONFLICT ON CONSTRAINT uniq_users_uuid
	DO UPDATE SET
	  user_agent = EXCLUDED.user_agent,
	  name = EXCLUDED.name,
	  gender = EXCLUDED.gender,
	  ip = EXCLUDED.ip,
	  age = EXCLUDED.age,
	  updated_at = NOW()
	RETURNING id`

	err := u.db.Get(&lastInsertId, insertSql,
		u.Uuid,
		u.UserAgent,
		u.Ip,
		u.Name,
		u.Gender,
		u.Age,
		u.Jwt,
	)

	if err != nil {
		return err
	}
	u.Id = lastInsertId

	return nil
}

func (u *User) Update() error {
	if err := u.validate(); err != nil {
		return err
	}

	updateSql := `UPDATE users SET
		user_agent = :ua,
		ip = :ip,
		name = :name,
		gender = :gender,
		age = :age,
		updated_at = NOW()
		WHERE uuid = :uuid`

	_, err := u.db.NamedExec(updateSql,
		map[string]interface{}{
			"ua":     u.UserAgent,
			"ip":     u.Ip,
			"name":   u.Name,
			"gender": u.Gender,
			"age":    u.Age,
			"uuid":   u.Uuid,
		})

	return err
}

func (u *User) RefreshToken() error {
	claims := jwt.StandardClaims{
		Subject:   u.Uuid,
		ExpiresAt: time.Now().Add(time.Hour * 24 * JWT_EXPIRES_DAYS).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(os.Getenv("FOURCHAT_SECRET_KEY")))

	if err != nil {
		return err
	}

	u.Jwt = tokenString

	return nil
}

func (u *User) Find(id string) (*User, error) {
	findStatement := `SELECT
		id,
		uuid,
		jwt,
		name,
		gender,
		age
	  FROM users WHERE uuid = $1`

	err := u.db.Get(u, findStatement, id)

	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	photo, err := NewUserPhoto(u.db).FindByUserId(u.Id)
	if err != nil {
		return nil, err
	} else if photo == nil {
		return u, nil
	}

	u.PhotoUrl, err = photo.PhotoUrl()
	if err != nil {
		return nil, err
	}

	return u, nil
}

func (u *User) validate() error {
	if u.Name == "" {
		return errors.New("Invalid name")
	}

	if u.Age < 16 || u.Age > 80 {
		return errors.New("Invalid age")
	}

	return nil
}
