package impl

import (
	"encoding/json"
	"gitlab.com/fourchat/app"
	"gitlab.com/isqad/fourchat/domain"
	"go.uber.org/zap"
	"net/http"
)

func CreateMessage(ctx *app.AppContext, w http.ResponseWriter, r *http.Request) {
	var message domain.Message

	decoder := json.NewDecoder(r.Body)
	logger := ctx.Log

	if err := decoder.Decode(&message); err != nil {
		logger.Error("decoding request body failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	message.AppContext = ctx
	message.UserId = ctx.CurrentUserId

	if err := message.Save(); err != nil {
		logger.Error("saving message failed", zap.Error(err))
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	resp, err := json.Marshal(message)
	if err != nil {
		logger.Error("marshaling message failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)

	ctx.EventBus.Pub <- []byte(resp)
}
