module gitlab.com/isqad/fourchat

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/valve v0.0.0-20170920024740-9e45288364f4
	github.com/google/uuid v1.1.0
	github.com/gorilla/mux v1.7.0
	github.com/gorilla/websocket v1.4.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.0.0
	github.com/segmentio/kafka-go v0.3.2
	gitlab.com/fourchat/app v0.2.0
	go.uber.org/zap v1.9.1
)
