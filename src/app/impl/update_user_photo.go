package impl

import (
	"gitlab.com/isqad/fourchat/domain"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"
	"go.uber.org/zap"
	"io"
	"net/http"
	"os"
	"path/filepath"
)

func UpdateUserPhoto(w http.ResponseWriter, r *http.Request) {
	logger, _ := appMiddleware.GetLog(r.Context())
	db, _ := appMiddleware.GetDb(r.Context())
	user, _ := appMiddleware.GetCurrentUser(r.Context())

	userPhoto := domain.NewUserPhoto(db)
	userPhoto.UserId = user.Id

	r.ParseMultipartForm(32 << 20)

	file, handler, err := r.FormFile("userPhoto")
	if err != nil {
		logger.Error("upload photo failed", zap.Error(err))
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	defer file.Close()

	userPhoto.FileName = handler.Filename
	err = userPhoto.Save()
	if err != nil {
		logger.Error("upload photo failed", zap.Error(err))
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	dirs, _ := userPhoto.PhotoIdPartitions()
	photoDir, _ := userPhoto.PhotoDir(domain.UserPhotoDir)

	oldFileNames := dirs[len(dirs)-1] + "_original*"
	oldFiles, err := filepath.Glob(photoDir + "/" + oldFileNames)
	if err != nil {
		logger.Error("upload photo failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for _, f := range oldFiles {
		if err := os.Remove(f); err != nil {
			logger.Error("upload photo failed", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	err = os.MkdirAll(photoDir, 0755)
	if err != nil {
		logger.Error("upload photo failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	photoPath, _ := userPhoto.PhotoPath(domain.UserPhotoDir)
	f, err := os.OpenFile(photoPath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		logger.Error("upload photo failed", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer f.Close()

	io.Copy(f, file)

	w.WriteHeader(http.StatusOK)
}
