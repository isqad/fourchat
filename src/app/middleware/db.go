package middleware

import (
	"context"
	"github.com/go-chi/chi/middleware"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // TODO: заменить на pgx
	"net/http"
)

var (
	ctxKeyDb = contextKey("Db")
)

func GetDb(ctx context.Context) (*sqlx.DB, bool) {
	l, ok := ctx.Value(ctxKeyDb).(*sqlx.DB)
	return l, ok
}

func NewDb(spec string) (*sqlx.DB, error) {
	return sqlx.Connect("postgres", spec)
}

func Db(c *sqlx.DB) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		h := func(w http.ResponseWriter, r *http.Request) {
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			r = r.WithContext(context.WithValue(r.Context(), ctxKeyDb, c))
			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(h)
	}
}
