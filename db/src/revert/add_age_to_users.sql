-- Revert ladung:add_age_to_users from pg

BEGIN;

  ALTER TABLE users DROP COLUMN age;

COMMIT;
