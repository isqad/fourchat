-- Deploy ladung:add_age_to_users to pg

BEGIN;

  ALTER TABLE users ADD COLUMN age integer NOT NULL;

  ALTER TABLE users ADD CONSTRAINT age_range CHECK (age > 12 AND age < 80);

COMMIT;
