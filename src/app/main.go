package main

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	//"github.com/go-chi/chi/middleware"
	"github.com/go-chi/valve"
	//"github.com/joho/godotenv"
	"gitlab.com/isqad/fourchat/impl"
	appMiddleware "gitlab.com/isqad/fourchat/middleware"
	"gitlab.com/isqad/fourchat/uc"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	//err := godotenv.Load("/api/.env." + os.Getenv("FOURCHAT_APP_ENV"))
	//if err != nil {
	//	log.Fatalf("Can't load .env for given ENV: %v, FOURCHAT_APP_ENV: %v", err, os.Getenv("FOURCHAT_APP_ENV"))
	//}

	valv := valve.New()
	baseCtx := valv.Context()
	logger, err := appMiddleware.NewZapLogger()
	if err != nil {
		log.Fatalf("Can't initialize zap logger: %v", err)
	}
	defer logger.Sync()

	errorLogger := log.New(&uc.FwdToZapWriter{Logger: logger}, "", 0)

	dbSpec := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		os.Getenv("FOURCHAT_APP_POSTGRES_USER"),
		os.Getenv("FOURCHAT_APP_POSTGRES_PASSWORD"),
		os.Getenv("FOURCHAT_APP_POSTGRES_HOST"),
		os.Getenv("FOURCHAT_APP_POSTGRES_PORT"),
		os.Getenv("FOURCHAT_APP_POSTGRES_DB"),
	)

	dbConn, err := appMiddleware.NewDb(dbSpec)
	if err != nil {
		log.Fatalf("Opening db connectoin failed: %v, dbSpec: %s", err, dbSpec)
	}
	defer dbConn.Close()

	err = dbConn.Ping()
	if err != nil {
		log.Fatalf("Db is not available: %v", err)
	}

	kafkaDialogRequestsWriter := appMiddleware.NewKafkaDialogRequestsWriter(errorLogger)
	defer kafkaDialogRequestsWriter.Close()

	r := chi.NewRouter()
	r.Use(appMiddleware.RealIP)
	r.Use(appMiddleware.ZapLogger(logger))
	r.Use(appMiddleware.Db(dbConn))
	r.Get("/", impl.HomePage)
	r.Route("/api/v1", func(r chi.Router) {
		r.Route("/dialog_requests", func(r chi.Router) {
			r.With(appMiddleware.AuthCurrentUser, appMiddleware.KafkaDialogRequestsWriter(kafkaDialogRequestsWriter)).
				Post("/", impl.CreateRequest)
		})

		r.Route("/users", func(r chi.Router) {
			r.Post("/", impl.CreateUser)

			r.With(appMiddleware.AuthCurrentUser).
				Get("/{uuid}", impl.ShowUser)

			r.With(appMiddleware.AuthCurrentUser).
				Put("/", impl.UpdateUser)

			r.With(appMiddleware.AuthCurrentUser).
				Put("/photos", impl.UpdateUserPhoto)
		})
	})

	listenAddr := os.Getenv("FOURCHAT_APP_BIND_ADDR")
	server := &http.Server{
		Addr:         listenAddr,
		Handler:      chi.ServerBaseContext(baseCtx, r),
		ErrorLog:     errorLogger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is a ^C, handle it
			log.Println("shutting down..")

			// first valv
			valv.Shutdown(20 * time.Second)

			// create context with timeout
			ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
			defer cancel()

			// start http shutdown
			server.Shutdown(ctx)

			// verify, in worst case call cancel via defer
			select {
			case <-time.After(21 * time.Second):
				log.Println("not all connections done")
			case <-ctx.Done():

			}
		}
	}()

	/**
	err = server.ListenAndServeTLS(
		os.Getenv("FOURCHAT_APP_SSL_CERT"),
		os.Getenv("FOURCHAT_APP_SSL_KEY"),
	)
	**/
	err = server.ListenAndServe()
	if err != nil {
		logger.Fatal("Start server is failed", zap.Error(err))
	}
}
