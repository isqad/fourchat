-- Deploy ladung:create_users to pg

BEGIN;

  CREATE TYPE gender_type AS ENUM ('male','female');
  CREATE TABLE users (
    id bigserial NOT NULL PRIMARY KEY,
    uuid varchar(36) NOT NULL,
    user_agent varchar(2096) NOT NULL,
    ip inet NOT NULL,
    jwt varchar(1024),
    name varchar(255) NOT NULL,
    gender gender_type NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
  );

  CREATE UNIQUE INDEX index_users_on_uuid ON users(uuid);
  CREATE UNIQUE INDEX index_users_on_jwt ON users(jwt);

COMMIT;
