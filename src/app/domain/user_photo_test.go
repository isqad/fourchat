package domain

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestUserPhotoSave(t *testing.T) {
	d := db()

	// Load fixtures
	u, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	p := NewUserPhoto(d)

	p.FileName = "test.png"
	p.UserId = u.Id

	err = p.Save()
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if p.Id == 0 {
		t.Errorf("Expected photo has id, but no: %v", p)
	}
}

func TestFindByUserId(t *testing.T) {
	d := db()
	// Load fixtures
	u, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	pFixture, err := UserPhotoFixture(d, u)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	p, err := NewUserPhoto(d).FindByUserId(u.Id)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if p == nil {
		t.Errorf("Expected UserPhoto, got: %v", p)
	}

	if p.Id != pFixture.Id {
		t.Errorf("Expected UserPhoto id, got: %v", p)
	}

	if p.FileName != pFixture.FileName {
		t.Errorf("Expected UserPhoto FileName, got: %v", p)
	}

	if p.UpdatedAt.Day() != time.Now().Day() {
		t.Errorf("Expected UserPhoto UpdatedAt, got: p: %v", p)
	}
}

func TestPhotoUrl(t *testing.T) {
	d := db()
	// Load fixtures
	u, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	_, err = UserPhotoFixture(d, u)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}
	p, err := NewUserPhoto(d).FindByUserId(u.Id)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	photoPath, err := p.PhotoPath(UserPhotoPrefixPath)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	expectedPhotoUrl := fmt.Sprintf(
		"http://img.fourchat.docker/rs/fit/512/512/sm/0/plain/local://%s?%d",
		photoPath,
		p.UpdatedAt.Unix(),
	)
	url, err := p.PhotoUrl()
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if url != expectedPhotoUrl {
		t.Errorf("Expected %v, got: %v", expectedPhotoUrl, url)
	}
}

func TestPhotoIdPartitions(t *testing.T) {
	d := db()
	// Load fixtures
	u, err := UserFixture(d)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	_, err = UserPhotoFixture(d, u)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}
	p, err := NewUserPhoto(d).FindByUserId(u.Id)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	idPart, err := p.PhotoIdPartitions()
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	idPartStr := strings.Join(idPart, "")
	idPartInt, err := strconv.ParseInt(idPartStr, 10, 64)
	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if idPartInt != p.Id {
		t.Errorf("Expected PhotoIdPartitions is same as UserPhoto Id, but no: %v", idPart)
	}
}
