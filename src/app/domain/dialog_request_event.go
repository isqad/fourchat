package domain

import (
	"fmt"
)

type DialogRequestType int

const (
	DialogRequestEventTypeCreate DialogRequestType = iota
	DialogRequestEventTypeDel
)

type DialogRequestEvent struct {
	Type          DialogRequestType `json:"type"`
	DialogRequest *DialogRequest    `json:"dialog_request"`
}

func NewDialogRequestCreateType(r *DialogRequest) *DialogRequestEvent {
	return &DialogRequestEvent{Type: DialogRequestEventTypeCreate, DialogRequest: r}
}

func NewDialogRequestDelType(r *DialogRequest) *DialogRequestEvent {
	return &DialogRequestEvent{Type: DialogRequestEventTypeDel, DialogRequest: r}
}

func (d *DialogRequestEvent) Key() string {
	return fmt.Sprintf("dialog-message-%d-%d", d.Type, d.DialogRequest.Id)
}
