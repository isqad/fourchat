-- Deploy ladung:add_updated_at_to_user_photos to pg

BEGIN;

  ALTER TABLE user_photos ADD COLUMN updated_at timestamp with time zone;

COMMIT;
