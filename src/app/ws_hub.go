package main

import (
	"gitlab.com/fourchat/app"
	"go.uber.org/zap"
)

const EVENT_BUS_CONSUMER = "WsHub"

type WsHub struct {
	// registered clients
	clients map[*WsClient]bool
	// messages from clients
	broadcast chan []byte
	// register requests from the clients
	register   chan *WsClient
	unregister chan *WsClient

	appContext *app.AppContext
}

func newHub(ctx *app.AppContext) *WsHub {
	return &WsHub{
		broadcast:  make(chan []byte),
		clients:    make(map[*WsClient]bool),
		register:   make(chan *WsClient),
		unregister: make(chan *WsClient),
		appContext: ctx,
	}
}

func (h *WsHub) run() {
	h.appContext.Log.Debug("WebSocket Hub is Running...")
	eventBus := h.appContext.EventBus
	eventConsumer := &app.EventConsumer{Bus: eventBus, Messages: make(chan []byte, 256)}
	eventBus.Register <- eventConsumer
	go eventConsumer.Consume()

	for {
		select {
		case client := <-h.register:
			h.appContext.Log.Debug("Regitered new client: ", zap.Any("clientId", client.userId))
			h.clients[client] = true
		case client := <-h.unregister:
			h.appContext.Log.Debug("Unregistered client: ", zap.Any("clientId", client.userId))
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			h.appContext.Log.Debug("Broadcast message: ", zap.String("msg", string(message[:])))
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		case event := <-eventConsumer.Messages:
			h.appContext.Log.Debug("Event from system bus: ", zap.String("msg", string(event[:])))
			// TODO: доработать, чтобы слать только нужным получателям, а не всем
			for client := range h.clients {
				select {
				case client.send <- event:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
